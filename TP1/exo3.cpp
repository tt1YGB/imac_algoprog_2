#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);


int search(int value, Array& array, int size)
{
    Context _("search", value, size); // do not care about this, it allow the display of call stack

    // your code
    // la fonction vérifie d'abord si la dernière cellule du tableau correspond à la valeur recherchée. 
    //Si c'est le cas, elle retourne l'index de cette cellule. Sinon, elle rappelle la fonction avec un tableau 
    //plus petit qui exclut la dernière cellule et continue la recherche.

    if (array[size - 1] == value) {
        return_and_display(size - 1);
    } else if (size > 1) {
        return_and_display(search(value, array, size - 1));
    } else {
        return_and_display( -1);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new SearchWindow(search); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}




