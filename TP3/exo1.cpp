#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

int binarySearch(Array& array, int toSearch)
{
	int debut = 0;
	int milieu;
	int fin = array.size();
    while (debut < fin) {
        milieu = (debut + fin) / 2;

        if (toSearch > array[milieu]) {
            debut = milieu + 1;
        } else if (toSearch < array[milieu]) {
            fin = milieu;
        } else {
            return milieu;
        }
    }

	return -1;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
