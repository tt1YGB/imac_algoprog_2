#include "tp1.h"
#include <QApplication>
#include <time.h>

bool isMandelbrot(Point z, int n, Point point){ //je l'ai modifier pour en faire un booléen pour que sa présence au sein de Mandelbrot soit plus parlant

    float tempX = z.x;
    float tempY = z.y;

    // z²
    z.x = (tempX * tempX - tempY * tempY);
    z.y = 2 * tempX * tempY;

    // z²+point
    z.x += point.x;
    z.y += point.y;

    float module = sqrt(z.x * z.x + z.y * z.y);
    //float moduleSquare = z.x * z.x + z.y * z.y;
    /* if (moduleSquare > 4) {
        return false;
    } else if (n == 0) {
        return true;
    } */

    if (module > 2) {
        return true;
    } else if (n == 0) {
        return false;
    }

    return isMandelbrot(z, n - 1, point);

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



