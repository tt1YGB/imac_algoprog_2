#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	// Pour chaque noeud, on crée un nouvel objet GraphNode
	for (int i = 0; i < nodeCount; i++) {
       GraphNode* node = new GraphNode(i);
       this->appendNewNode(node);
    }
	// On parcourt la matrice d'adjacence pour ajouter les arêtes correspondantes
    for (int i = 0; i < nodeCount; i++) {
        for (int j = 0; j < nodeCount; j++) {
            if (adjacencies[i][j] != 0) {
				// Si la valeur de la matrice est différente de 0, cela signifie qu'il y a une arête entre les noeuds i et j, donc on ajoute l'arête entre ces deux noeuds
                this->nodes[i]->appendNewEdge(nodes[j]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	// On marque le noeud initial comme visité
	visited[first->value] = 1;
    nodes[nodesSize++] = first;
    Edge* currentEdge = first->edges;
	// On boucle sur toutes les arêtes du noeud visité pour visiter les noeuds adjacents
    while (currentEdge != nullptr) {
    	if (!visited[currentEdge->destination->value]) { // Si le noeud de destination de l'arête n'a pas encore été visité
        	deepTravel(currentEdge->destination, nodes, nodesSize, visited); // On visite le noeud de destination récursivement
    	}
    currentEdge = currentEdge->next; // On passe à l'arête suivante du noeud visité
}

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);
    Edge* currentEdge = first->edges; // On récupère la première arête du noeud initial

	while (!nodeQueue.empty()) {
		GraphNode* cursorNode = nodeQueue.front(); // On récupère le premier élément de la file
		nodeQueue.pop(); // On enlève le noeud visité de la file
		visited[cursorNode->value] = 1; // On marque le noeud visité comme visité
		nodes[nodesSize++] = cursorNode;

		// On boucle sur toutes les arêtes du noeud visité pour visiter les noeuds adjacents
		while (currentEdge != nullptr) {
			if (!visited[currentEdge->destination->value]) { // Si le noeud de destination de l'arête n'a pas encore été visité
				nodeQueue.push(currentEdge->destination); // On ajoute le noeud de destination à la file pour le visiter plus tard
			}
			currentEdge = currentEdge->next; // On passe à l'arête suivante du noeud visité
		}

		if (!nodeQueue.empty()) { // Si la file n'est pas vide, on récupère la première arête du prochain noeud à visiter
			currentEdge = nodeQueue.front()->edges;
		}
	}

}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);

	while (!nodeQueue.empty()) {
		GraphNode *cursorNode = nodeQueue.front(); // On récupère le premier élément de la file
		nodeQueue.pop(); // On enlève le noeud visité de la file
		visited[cursorNode->value] = true; // On marque le noeud visité comme visité

		// On boucle sur toutes les arêtes du noeud visité pour visiter les noeuds adjacents
		for (Edge *currentEdge = cursorNode->edges; currentEdge != NULL; currentEdge = currentEdge->next) {
			if (!visited[currentEdge->destination->value]) { // Si le noeud de destination de l'arête n'a pas encore été visité
				nodeQueue.push(currentEdge->destination); // On ajoute le noeud de destination à la file pour le visiter plus tard
			}
			else if (currentEdge->destination == first){ // Si on retombe sur le noeud initial, on a détecté un cycle
				return true;
			}
		}
	}
	return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
