#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */
void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    indexMin = -1; 
    indexMax = -1;
    int debut = 0;
    int fin = array.size() - 1;

    // First binary search loop to find indexMax
    while (debut <= fin) {
        int milieu = (debut + fin) / 2;
        if (toSearch > array[milieu]) {
            debut = milieu + 1;
        } else if (toSearch < array[milieu]) {
            fin = milieu - 1;
        } else {
            indexMax = milieu;
            debut = milieu + 1;
        }
    }

    debut = 0;
    fin = indexMax;

    // Second binary search loop to find indexMin
    while (debut <= fin) {
        int milieu = (debut + fin) / 2;
        if (toSearch > array[milieu]) {
            debut = milieu + 1;
        } else if (toSearch < array[milieu]) {
            fin = milieu - 1;
        } else {
            indexMin = milieu;
            fin = milieu - 1;
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
