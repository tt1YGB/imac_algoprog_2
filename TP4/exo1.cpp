#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

//leftChildIndex et rightChildIndex calculent l'index des enfants gauche et droit d'un nœud, respectivement, dans un tableau en implémentant un tas binaire
int Heap::leftChildIndex(int nodeIndex)
{
    return (nodeIndex*2+1);
}

int Heap::rightChildIndex(int nodeIndex)
{
    return (nodeIndex*2+2);
}

 //insère un nœud dans un tas binaire à la fin du tableau et le remonte jusqu'à sa position correcte en échangeant le nœud avec son parent si nécessaire.
void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
	(*this)[i] = value;
	while (i>0 && this->get(i)>this->get((i-1)/2)) {
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}
 
 //réorganise un tas binaire à partir d'un nœud donné en échangeant le nœud avec son enfant le plus grand si nécessaire, puis en appelant récursivement heapify avec le nouvel index du nœud.
void Heap::heapify(int heapSize, int nodeIndex)
{
    int leftIndex = leftChildIndex(nodeIndex);
    int rightIndex = rightChildIndex(nodeIndex);
    int largestIndex = nodeIndex;

    if (leftIndex < heapSize && get(leftIndex) > get(largestIndex))
        largestIndex = leftIndex;
    
    if (rightIndex < heapSize && get(rightIndex) > get(largestIndex))
        largestIndex = rightIndex;
    
    if (largestIndex != nodeIndex) {
        swap(nodeIndex, largestIndex);
        heapify(heapSize, largestIndex);
    }
}


//crée un tas binaire à partir d'un tableau de nombres donné en appelant insertHeapNode pour chaque élément.
void Heap::buildHeap(Array& numbers)
{
	for (size_t i = 0; i < numbers.size(); i++) {
        insertHeapNode(i, numbers[i]);
    }
}

//trie les éléments d'un tas binaire en échangeant le premier élément (le plus grand) avec le dernier, puis en appelant heapify sur le premier élément pour réorganiser le tas binaire. 
// itère ensuite en réduisant la taille du tas binaire d'une unité à chaque étape jusqu'à ce que le tas binaire soit vide.
void Heap::heapSort()
{
	for (size_t i = this->size() - 1; i > 0; i--) {
        swap(0, i);
        heapify(i, 0);
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
