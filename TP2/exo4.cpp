#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if (size <= 1) {
        return;
    }

	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
    uint pivot = (size - 1) / 2;
    int ValeurPivot = toSort[pivot];
    uint lowerSize = 0 ; // effectives sizes
    uint greaterSize = 0; // effectives sizes

	// split
	for (uint i = 0; i < size; i++) {
        if (pivot != i) {
            if (toSort[i] < ValeurPivot) { //lowers ⇐ [n ∈ t tel que n < pivot]
                lowerArray[lowerSize] = toSort[i];
                lowerSize++;
            } else { //greaters ⇐ [n ∈ t tel que n > pivot]
                greaterArray[greaterSize] = toSort[i];
                greaterSize++;
            }
        }
    }
	
	// recursiv sort of lowerArray and greaterArray : le tri entre les lowers et les greaters
	recursivQuickSort(lowerArray, lowerSize);
    recursivQuickSort(greaterArray, greaterSize);

    for (uint i = 0; i < lowerSize; i++) {
        toSort[i] = lowerArray[i];
    }

		// merge : fusionner lowers, pivot et greaters
    toSort[lowerSize] = ValeurPivot;
    for (uint i = 0; i < greaterSize; i++) {
        toSort[i + 1 + lowerSize] = greaterArray[i]; // + 1 il ne faut pas oublier le pivot
    }
    
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	int elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
