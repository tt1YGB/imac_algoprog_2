#include <time.h>
#include <stdio.h>

#include <QApplication>
#include <QDebug>

#include "tp3.h"
#include "tp4.h"
#include "tp4_exo2.h"
#include "HuffmanNode.h"

_TestMainWindow* w1 = nullptr;
using std::size_t;
using std::string;

void processCharFrequences(string data, Array& frequences);
void buildHuffmanHeap(const Array& frequences, HuffmanHeap& priorityMinHeap, int& heapSize);
HuffmanNode* makeHuffmanSubTree(HuffmanNode* rightNode, HuffmanNode* leftNode);
HuffmanNode* buildHuffmanTree(HuffmanHeap& priorityMinHeap, int heapSize);

string huffmanEncode(const string& toEncode, HuffmanNode* huffmanTree);
string huffmanDecode(const string& toDecode, const HuffmanNode& huffmanTreeRoot);

void main_function(HuffmanNode*& huffmanTree)
{
    string data = "Ouesh, bien ou bien ? Ceci est une chaine de caracteres sans grand interet";

    // this array store each caracter frequences indexed by their ascii code
    Array characterFrequences(256);
    characterFrequences.fill(0);
    // this array store each caracter code indexed by their ascii code
    string characterCodes[256];
    HuffmanHeap priorityMinHeap;
    int heapSize = 0;

    processCharFrequences(data, characterFrequences);
    displayCharacterFrequences(characterFrequences);
    buildHuffmanHeap(characterFrequences, priorityMinHeap, heapSize);
    qDebug() << priorityMinHeap.toString().toStdString().c_str();

    huffmanTree = buildHuffmanTree(priorityMinHeap, heapSize);
    huffmanTree->processCodes("");
    string encoded = huffmanEncode(data, huffmanTree);
    string decoded = huffmanDecode(encoded, *huffmanTree);

    qDebug("Encoded: %s\n", encoded.c_str());
    qDebug("Decoded: %s\n", decoded.c_str());

}

void processCharFrequences(string data, Array& frequences)
{
    // Boucle sur chaque caractère de la chaîne "data"
    for (int i = 0; i < data.size(); i++) {
        // Récupération du code ASCII du caractère courant
        int asciicode = (int) data[i];
        // Incrémentation
        frequences[asciicode]++;
    }
}

void HuffmanHeap::insertHeapNode(int heapSize, HuffmanNode* newNode)
{
    int i = heapSize; // initialise l'index i à la taille du tas
    this->set(heapSize, newNode); // place le nouveau noeud à la fin du tas
    // Effectue une opération pour rétablir l'ordre du tas
    while (i > 0 && this->get(i)->frequences < this->get((i - 1) / 2)->frequences) {
        swap(i, (i - 1) / 2);
        i = (i - 1) / 2;
    }
}


void buildHuffmanHeap(const Array& frequences, HuffmanHeap& priorityMinHeap, int& heapSize)
{
    //initialisation
    heapSize = 0;
    // Parcourt les 256 caractères possibles
    for (int i = 0; i < 256; i++) {
        if (frequences[i] != 0) {
            // Crée un nouveau noeud Huffman avec le caractère et sa fréquence.
            HuffmanNode* newNode = new HuffmanNode(i, frequences[i]);
            // Insère le nouveau noeud
            priorityMinHeap.insertHeapNode(heapSize, newNode);
            heapSize++;
        }
    }
}

void HuffmanHeap::heapify(int heapSize, int nodeIndex)
{
    int smallestIndex = nodeIndex;
    // On calcule les indices des enfants gauche et droit
    int leftChildIndex = 2 * nodeIndex + 1;
    int rightChildIndex = 2 * nodeIndex + 2;

    // Si l'enfant gauche a une fréquence plus petite que le noeud courant, on met à jour l'index du plus petit
    if (leftChildIndex < heapSize && this->get(leftChildIndex)->frequences < this->get(smallestIndex)->frequences) {
        smallestIndex = leftChildIndex;
    }

    // Si l'enfant droit a une fréquence plus petite que le plus petit des deux noeuds jusqu'à présent, on met à jour l'index du plus petit
    if (rightChildIndex < heapSize && this->get(rightChildIndex)->frequences < this->get(smallestIndex)->frequences) {
        smallestIndex = rightChildIndex;
    }
    // Si le plus petit noeud n'est pas le noeud courant, on échange les deux noeuds et on rappelle la fonction heapify pour vérifier la propriété du tas
    if (smallestIndex != nodeIndex) {
        this->swap(nodeIndex, smallestIndex);
        heapify(heapSize, smallestIndex);
    }
}

HuffmanNode* HuffmanHeap::extractMinNode(int heapSize)
{
    if (heapSize <= 0) {
        return nullptr;
    }
    // Récupère le noud avec la fréquence la plus basse (au top du tas)
    HuffmanNode* minNode = this->get(0);
    // Échange le noeud le plus bas avec le last noeud du tas(plus tas-1)
    this->swap(0, heapSize - 1);
    heapSize--;
    // Restaure la propriété de tas
    this->heapify(heapSize, 0);
    // Renvoie le noeud avec la fréquence la plus basse
    return minNode;
}

HuffmanNode* makeHuffmanSubTree(HuffmanNode* rightNode, HuffmanNode* leftNode)
{
    // Création du noeud parent avec un caractère fictif '\0' et une fréquence qui est la somme des fréquences des sous-arbres
    HuffmanNode* parentNode = new HuffmanNode('\0', rightNode->frequences + leftNode->frequences);
    // Liaison du sous-arbre droit au nouveau noeud parent
    parentNode->right = rightNode;
    // Liaison du sous-arbre gauche au nouveau noeud parent
    parentNode->left = leftNode;
    // Retourne le nouveau noeud parent créé
    return parentNode;
}


HuffmanNode* buildHuffmanTree(HuffmanHeap& priorityMinHeap, int heapSize)
{
    //tant qu'il y a plus d'un noeud dans le tas, on continue de construire l'arbre
    while (heapSize > 1) {
        // On extrait le noeud avec la plus petite fréquence
        HuffmanNode* left = priorityMinHeap.extractMinNode(heapSize);
        heapSize--;
        // On extrait le noeud avec la deuxième plus petite fréquence
        HuffmanNode* right = priorityMinHeap.extractMinNode(heapSize);
        heapSize--;
        // On crée un nouveau noeud parent qui contient les deux noeuds extraits
        HuffmanNode* parent = makeHuffmanSubTree(left, right);
        // On insère le nouveau noeud parent dans le tas
        priorityMinHeap.insertHeapNode(heapSize, parent);
        heapSize++;
    }
    // On retourne le dernier noeud restant dans le tas
    return priorityMinHeap.extractMinNode(heapSize);
}


void HuffmanNode::processCodes(const std::string& baseCode)
{
    // Si le noeud est une feuille, on assigne le code de base
    if (this->left == nullptr && this->right == nullptr) {
        this->code = baseCode;
        return;
    }
    // Si le noeud a un enfant gauche, on concatène un "0" au code de base et on appelle récursivement la fonction pour l'enfant gauche
    if (this->left != nullptr) {
        std::string leftCode = baseCode + "0";
        this->left->processCodes(leftCode);
    }
    // Si le noeud a un enfant droit, on concatène un "1" au code de base et on appelle récursivement la fonction pour l'enfant droit
    if (this->right != nullptr) {
        std::string rightCode = baseCode + "1";
        this->right->processCodes(rightCode);
    }
}

void HuffmanNode::fillCharactersArray(std::string charactersCodes[])
{
    if (!this->left && !this->right)
        charactersCodes[this->character] = this->code;
    else {
        if (this->left)
            this->left->fillCharactersArray(charactersCodes);
        if (this->right)
            this->right->fillCharactersArray(charactersCodes);
    }
}

string huffmanEncode(const string& toEncode, HuffmanNode* huffmanTree)
{
    // Créer un tableau qui stocke les codes de Huffman
    std::string charactersCodes[256];

    // Remplir le tableau
    huffmanTree->fillCharactersArray(charactersCodes);

    // Créer une chaîne de caractères `encoded` pour stocker la chaîne de caractères encodée
    string encoded = "";

    for (char c : toEncode) {
        encoded += charactersCodes[(unsigned char)c];
    }

    // Retourner la chaîne de caractères encodée
    return encoded;
}

string huffmanDecode(const string& toDecode, const HuffmanNode& huffmanTreeRoot)
{
    //initialisatio
    string decoded = "";
    HuffmanNode* currentNode = const_cast<HuffmanNode*>(&huffmanTreeRoot);

    for (char c : toDecode) {
        if (c == '0') {
            //on descend dans l'arbre à gauche
            currentNode = static_cast<HuffmanNode*>(currentNode->get_left_child());
        } 
        else if (c == '1') {
            //on descend dans l'arbre à droite
            currentNode = static_cast<HuffmanNode*>(currentNode->get_right_child());
        } 

        // Si on est arrivé à une feuille de l'arbre, on a trouvé le caractère correspondant
        if (currentNode->isLeaf()) {
            decoded += currentNode->character;
            currentNode = const_cast<HuffmanNode*>(&huffmanTreeRoot);
        }
    }
    return decoded;
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Array::wait_for_operations = false;
    w1 = new HuffmanMainWindow(main_function);
    w1->show();
    return a.exec();
}

